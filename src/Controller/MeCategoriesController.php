<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MeCategoriesController extends AbstractController
{
    public function __construct(private Security $security, private CategoryRepository $categoryRepository)
    {
    }

    public function __invoke()
    {
        $user = $this->security->getUser();
        $categories = $this->categoryRepository->findAll();
        $expenses = $user->getExpenses();
        return $expenses;
    }
}
