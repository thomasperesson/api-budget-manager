<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ExpenseRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ExpenseRepository::class)
 */
#[ApiResource(
    normalizationContext: ['groups' => ['expense:read']],
    denormalizationContext: ['groups' => ['expense:write']],
    attributes: ["security" => "is_granted('ROLE_USER')"],
    collectionOperations: [
        "get" => ["security" => "is_granted('ROLE_ADMIN')"],
        "post" => ["security" => "is_granted('ROLE_USER')"],
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('ROLE_ADMIN') or object.getUser() == user",

        ],
        "put" => ["security" => "is_granted('ROLE_ADMIN') or object.getUser() == user"],
        "delete" => ["security" => "is_granted('ROLE_ADMIN') or object.getUser() == user"],
    ]
),
ApiFilter(DateFilter::class, properties: ['expenseDate'])]
class Expense
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(["expense:read", "user:read", "category:read"])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["expense:read", "user:read", "category:read"])]
    private $title;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(["expense:read", "user:read", "category:read"])]
    private $expenseDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["expense:read", "user:read", "category:read"])]
    private $description;

    /**
     * @var User The owner
     * 
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="expenses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="expenses")
     */
    #[Groups(["expense:read", "user:read"])]
    private $category;

    /**
     * @ORM\Column(type="float")
     */
    #[Groups(["expense:read", "user:read", "category:read"])]
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getExpenseDate(): ?\DateTimeImmutable
    {
        return $this->expenseDate;
    }

    public function setExpenseDate(\DateTimeImmutable $expenseDate): self
    {
        $this->expenseDate = $expenseDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
