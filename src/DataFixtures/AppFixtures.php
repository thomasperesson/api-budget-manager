<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Expense;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    private function createUsers(ObjectManager $manager, $faker) {
        $user = new User();

            $password = $this->hasher->hashPassword($user, 'user');
            $user->setFirstname($faker->firstname())
                ->setLastname($faker->lastname())
                ->setEmail($faker->email())
                ->setPassword($password);
            $manager->persist($user);

            return $user;
    }

    private function createExpenses(ObjectManager $manager, $faker, $user) {
        $expense = new Expense();

                $expense->setTitle($faker->word())
                    ->setAmount($faker->randomFloat(2, -500, 0))
                    ->setExpenseDate(\DateTimeImmutable::createFromMutable($faker->dateTimeBetween('-1 year')))
                    ->setDescription($faker->sentence())
                    ->setUser($user);

                $manager->persist($expense);
    }

    public function load(ObjectManager $manager): void
    {
        // Creation User
        $faker = Factory::create('fr_FR');
        $userArr = [];
        for ($i = 1; $i <= 10; $i++) {
            
            $userArr[] = $this->createUsers($manager, $faker);
        }

        foreach($userArr as $user) {
            for ($i = 1; $i <= 10; $i++) {
                $this->createExpenses($manager, $faker, $user);
            }
        }

        for ($i = 1; $i <= 10; $i++) {
            $category = new Category();

            $category->setTitle($faker->word())
                ->setLogo($faker->imageUrl());
            $manager->persist($category);
        }
        $manager->flush();
    }
}
